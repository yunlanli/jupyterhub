# JupyterHub Deployment
The following tutorial walks through deploying JupyterHub on Ubuntu 20.04LTS
and each singleuser notebook server will be started in a docker container.

## Pre-requisites
- A Linux/Unix based system
- Python >= 3.6
- nodejs/npm installed using your operating system's package manager
- docker

## Installation
For demonstration, we'll use python3.8 as our python interpreter to
install packages. If you are using a different version of python3, simply
replace it with your version of python3.

1. Create a python virtual environment for JupyterHub under `/opt`, a
   standard location for add-on software packages under Linux's FHS:

   ```bash
   sudo /usr/bin/python3.8 -m venv /opt/jupyterhub
   ```

   > If venv is not installed with your python3, on Ubuntu systems, you can
   > install it with the APT package manager as follows:
   >
   > ```bash
   > sudo apt-get update && sudo apt-get install python3.8-venv
   > ```

1. Install jupyterhub into the virtual environment we just created:

   ```bash
   sudo /opt/jupyterhub/bin/python3.8 -m pip install jupyterhub
   ```

   > By providing the full path to the python interpreter, we ensure that the
   > correct interpreter will be used to run pip and as a result, the packages
   > will be installed into our virtual environment even though we haven't
   > activated the virtual environment.
   >
   > Note that activating the virtual environment first, and install jupyterhub
   > via `python3.8 -m pip install jupyterhub` will not work because only the
   > root user can modify `/opt`. Moreover, using `sudo python3.8 -m
   > install jupyterhub` will also fail as it runs in the context of the root
   > user and will cause jupyterhub to be installed system wide.

1. Install dockerspawner and netifaces into the virtual environment. These
   packages will allow us to start singleuser notebook servers in docker
   container.

   ```bash
   sudo /opt/jupyterhub/bin/python3.8 -m pip install dockerspawner netifaces
   ```

1. Install configurable-http-proxy, which will be used as the proxy for
   JupyterHub.

   ```bash
   npm install -g configurable-http-proxy
   ```

1. We'll use our customized NISTAuthenticator for authenticating users with
   a HTTP header in JupyterHub. It has been published to [PyPI][1] and could
   be installed directly with `pip`.

   ```bash
   sudo /opt/jupyterhub/bin/python3.8 -m pip install nistauthenticator
   ```

   > For details of `nistautheticator`, please refer to the README.md in
   > the nistautenticator folder.

1. Build the customized docker image for spawning singleuser notebook
   servers. Run the following command under
   `/jupyter-docker-stacks/base-notebook`, where `/` is the root of this
   repository.

   ```bash
   sudo docker build -t nist-notebook:hub-1.3.0 .
   ```

1. Lastly, copy the JupyterHub configuration into the virtual environment:
  
   ```bash
   sudo mkdir -p /opt/jupyterhub/etc/jupyterhub
   sudo cp jupyterhub_config.py  /opt/jupyterhub/etc/jupyterhub/
   ```


## Setup systemd Service
After successful installation of JupyterHub, it can be started by

```bash
<path-to-jupyterhub-binary> -f <path-to-jupyterhub-config-file>
```

To allow JupyterHub to start automatically on system boot, we create a
systemd service file for JupyterHub under `/opt/jupyterhub/etc/systemd`:

```bash
sudo mkdir -p /opt/jupyterhub/etc/systemd
```

Create a file named `jupyterhub.service` and put the following content into
it with your favorite text editor.

```
[Unit]
Description=JupyterHub
After=syslog.target network.target

[Service]
User=root
Environment="PATH=/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/opt/jupyterhub/bin"
# the config file we just created will be used to config JupyterHub
ExecStart=/opt/jupyterhub/bin/jupyterhub -f
/opt/jupyterhub/etc/jupyterhub/jupyterhub_config.py

[Install]
WantedBy=multi-user.target
```

Make systemd aware of our service file and start running JupyterHub:

```bash
sudo ln -s /opt/jupyterhub/etc/systemd/jupyterhub.service   /etc/systemd/system/jupyterhub.service

sudo systemctl daemon-reload  # reload systemd manager configuration
sudo systemctl enable jupyterhub.service  # enable jupyterhub to start on boot
sudo systemctl start jupyterhub.service
```

Now, we should see JupyterHub is active(running) if we inspect its status
via:

```bash
sudo systemctl status jupyterhub.service
```

To inspect the log produced by JupyterHub, run:

```bash
sudo journalctl -fu jupyterhub
```

[1]: https://pypi.org/project/nistauthenticator/ "PyPI nistautenticator"
