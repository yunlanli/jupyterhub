c = get_config()

c.JupyterHub.bind_url = 'http://:8888/jupyter'

# Authenticator configuration
c.JupyterHub.authenticator_class = 'nistauthenticator.NISTAuthenticator'
c.Authenticator.admin_users = {
        "jj2513@columbia.edu",
        "ab4659@columbia.edu",
        "yl4387@columbia.edu"
        }
c.NISTAuthenticator.user_header = 'x-authorized-user'
c.NISTAuthenticator.logoutURL = 'https://mcv-testbed.cs.columbia.edu/'
c.JupyterHub.shutdown_on_logout = True

# spawner configuration
c.JupyterHub.spawner_class = 'dockerspawner.DockerSpawner'
# The docker instances need access to the Hub, so the default loopback port doesn't work:
from jupyter_client.localinterfaces import public_ips
import netifaces

# Base Station uses docker 19.04 on Linux, which doesn't support docker.host.internal
# we find it here using netifaces
def get_docker_host():
    ip = []

    try:
        docker_ipv4 = netifaces.ifaddresses('docker0').get(netifaces.AF_INET, [])
        for entry in docker_ipv4:
            addr = entry.get("addr")
            if addr:
                ip.append(addr)
                break;

        # no ip found
        raise Exception()
    except:
        print('[ jupyterhub_config.py ] unable to find docker0 inet4 internal ip\n')

    return ip

c.JupyterHub.hub_ip = public_ips()[0]
docker_host = get_docker_host()
if len(docker_host):
    c.DockerSpawner.extra_host_config = {
            "extra_hosts": {
                'docker.host.internal': docker_host[0]
                }
            }

c.DockerSpawner.image = 'nist-notebook:hub-1.3.0'
c.DockerSpawner.remove = True

notebook_dir = '/home/jovyan/nist'
user_file_dir = f'{notebook_dir}/notebook'
media_file_dir = f'{notebook_dir}/media'
share_dir = f'{notebook_dir}/share'

c.DockerSpawner.notebook_dir = notebook_dir
c.DockerSpawner.volumes = {
        'jupyterhub-{username}': user_file_dir,
        'jupyterhub-shared': share_dir,
        '/srv/lmr/wavs': {
            'bind': media_file_dir,
            'mode': 'ro'
            },
        }

import os

# extra environment variables in single-user server
#c.DockerSpawner.environment = dict(
#        MONGODB_USR  = os.environ['MONGODB_USR'],
#        MONGODB_NAME = os.environ['MONGODB_NAME'],
#        MONGODB_PWD  = os.environ['MONGODB_PWD']
#        )
